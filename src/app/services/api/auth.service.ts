import {Injectable} from '@angular/core';
import {Api} from '../libraries/api';

@Injectable()
export class AuthService {

    constructor(private api: Api) { }

    login(body) {
        return this.api.postJson('auth/login', body);
    }

    me() {
        return this.api.get('auth/me');
    }

    logout() {
        return this.api.get('auth/logout');
    }
}
