import {Injectable} from '@angular/core';
import {Api} from '../libraries/api';
import {Observable} from 'rxjs/internal/Observable';

@Injectable()
export class LeaseService {

    constructor(private api: Api) {}

    public getById(transactionId: number): Observable<any> {
        return this.api.get(`lease/transactions/${transactionId}`);
    }

    public createLeaseTransaction(body: any): Observable<any> {
        return this.api.postJson(`lease/checkout`, body);
    }
}
