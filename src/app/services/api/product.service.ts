import {Injectable} from '@angular/core';
import {Api} from '../libraries/api';
import {Observable} from 'rxjs/internal/Observable';

@Injectable()

export class ProductService {

    constructor(private api: Api) {
    }

    public getProductDetail(productId: number): Observable<any> {
        return this.api.get(`products/${productId}`);
    }

    // public getProductbyId(productId: number)
}
