import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { Page404Component } from './page404/page404.component';

const routes: Routes = [
    {
        path: '',
        loadChildren: './web/web.module#WebModule'
    },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule'
    },
    {
        path: 'checkout',
        loadChildren: './payment/payment.module#PaymentModule'
    },
    {
        path: '**',
        component: Page404Component
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
