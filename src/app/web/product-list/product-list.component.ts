import {Component, OnInit} from '@angular/core';
import {Api} from '../../services/libraries/api';
import {tap} from 'rxjs/operators';
import {mapToData} from '../../services/transformers/response.transformer';

@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss']
})

export class ProductListComponent implements OnInit {

    public products: any;
    public titletest = 'haha';

    constructor(private api: Api) {
    }

    ngOnInit() {
        this.api.get('products')
            .pipe(
                mapToData(),
                tap((products: any[]) => {
                    this.products = products;
                }),
            )
            .subscribe();
    }

}
