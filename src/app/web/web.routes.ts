import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {WebTemplateComponent} from './web.template';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductDetailsComponent} from './product-details/product-details.component';
import {CartComponent} from './cart/cart.component';
import {OrderComponent} from './order/order.component';

const routes: Routes = [
    {
        path: '',
        component: WebTemplateComponent,
        children: [
            {
                path: 'cart',
                component: CartComponent
            },
            {
                path: '',
                component: HomeComponent
            },
            {
                path: 'products',
                component: ProductListComponent
            },
            {
                path: 'products/:id',
                component: ProductDetailsComponent
            },
            {
                path: 'order',
                component: OrderComponent
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WebRoutingModule {
}
