import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PaymentTemplateComponent} from './payment.template';
import {CheckoutComponent} from './checkout/checkout.component';

const routes: Routes = [
    {
        path: '',
        component: PaymentTemplateComponent,
        children: [
            {
                path: '',
                component: CheckoutComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PaymentRoutingModule {
}
