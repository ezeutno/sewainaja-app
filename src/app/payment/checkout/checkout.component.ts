import {Component, OnInit, ViewChild} from '@angular/core';
import {ElementOptions, ElementsOptions, StripeCardComponent, StripeService} from 'ngx-stripe';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PaymentService} from '../../services/api/payment.service';
import {switchMap, tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {LeaseService} from '../../services/api/lease.service';
import {mapToData} from '../../services/transformers/response.transformer';
import {forkJoin} from 'rxjs';

@Component({
    selector: 'app-checkout-page',
    templateUrl: './checkout.component.html',
    styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
    @ViewChild(StripeCardComponent) card: StripeCardComponent;

    cardOptions: ElementOptions = {
        style: {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        },
    };

    elementsOptions: ElementsOptions = {
        locale: 'en'
    };

    stripeForm: FormGroup;

    public transactionSummary: any;
    public orderId: number;

    constructor(
        private fb: FormBuilder,
        private stripeService: StripeService,
        private paymentService: PaymentService,
        private activatedRoute: ActivatedRoute,
        private leaseService: LeaseService
    ) {

    }

    public buy(): void {
        const cardDataOptions: any = {
            currency: 'IDR',
            email: 'test@gmail.com'
        };

        /*forkJoin(this.stripeService.createToken(this.card.getCard(), cardDataOptions), this.activatedRoute.queryParams)
            .pipe(
                switchMap(([stripeResult, queryParams]) => {
                    const payment = {
                        checkout: stripeResult,
                        transaction_id: queryParams.order_id
                    };

                    return this.paymentService.pay(payment).pipe(
                        tap(paymentResult => {
                            console.log(paymentResult);
                        }),
                    );
                }),
            ).subscribe();*/

        this.stripeService.createToken(this.card.getCard(), cardDataOptions)
            .pipe(
                switchMap(stripeResult => {
                    const payment = {
                        checkout: stripeResult,
                        transaction_id: this.orderId
                    };

                    return this.paymentService.pay(payment).pipe(
                        tap(paymentResult => {
                            console.log(paymentResult);
                        }),
                    );
                }),
            ).subscribe();
    }

    public ngOnInit(): void {
        this.stripeForm = this.fb.group({});

        this.activatedRoute.queryParams.pipe(
            switchMap(params => {
                this.orderId = params.order_id;

                return this.leaseService.getById(this.orderId).pipe(
                    mapToData(),
                    tap(transaction => {
                        this.transactionSummary = transaction;
                    },
                    err => {
                        console.log(err);
                    }),
                );
            }),
        ).subscribe();
    }
}
